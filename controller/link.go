package controller

import (
	"fmt"
	"log"
	"net/http"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	database "gitlab.com/mkadit/go-url/database/sqlc"
	"gitlab.com/mkadit/go-url/service"
)

type Link struct {
	svc *service.Link
}
type UrlForm struct {
	Long_url  string `json:"long_url"`
	Short_url string `json:"short_url"`
}
type UserForm struct {
	User     string `json:"username"`
	Password string `json:"password"`
}

func NewLinkController(svc *service.Link) *Link {
	return &Link{svc}
}

func (ctrl *Link) Hello(ctx *gin.Context) {
	jwtClaims := jwt.ExtractClaims(ctx)
	username := jwtClaims["username"].(string)
	id := jwtClaims["ID"].(float64)
	ctx.JSON(http.StatusOK, gin.H{
		"username": username,
		"id":       id,
		"text":     "Hello World.",
	})

}

func (ctrl *Link) GetUsers(ctx *gin.Context) {
	users, err := ctrl.svc.GetUsers()
	if err != nil {
		log.Fatal("Unable to get list of users!")
	}
	ctx.IndentedJSON(http.StatusOK, users)
	return
}

func (ctrl *Link) GetUser(ctx *gin.Context) {
	jwtClaims := jwt.ExtractClaims(ctx)
	username := jwtClaims["username"].(string)
	user, err := ctrl.svc.GetUserByName(username)
	if err != nil {
		log.Fatal("Unable to get user!")
	}
	// fmt.Println(util.CheckPassword(password, user.Password))
	ctx.IndentedJSON(http.StatusOK, user)
	return
}

func (ctrl *Link) CreateUser(ctx *gin.Context) {
	var userCreate UserForm
	err := ctx.ShouldBindJSON(&userCreate)
	if err != nil {
		ctx.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	_, err = ctrl.svc.GetUserByName(userCreate.User)
	if err == nil {
		log.Fatal("User already exist!")
	}
	user, err := ctrl.svc.CreateUser(userCreate.User, userCreate.Password)
	if err != nil {
		log.Fatal("User is invalid!")
	}
	userRequest := database.CreateUserParams{
		Username: user.Username,
		Password: user.Password,
	}
	ctx.IndentedJSON(http.StatusOK, userRequest)
	return

}

func (ctrl *Link) DeleteUser(ctx *gin.Context) {
	username := ctx.Param("username")
	err := ctrl.svc.DeleteUser(username)
	if err != nil {
		log.Fatal("User doesn't exist!")
	}
	ctx.IndentedJSON(http.StatusOK, fmt.Sprintf("%s has been successfully deleted", username))
	return
}

func (ctrl *Link) GetAllLinksPublic(ctx *gin.Context) {
	urls, err := ctrl.svc.GetAllLinksPublic()
	if err != nil {
		log.Fatal("Unable to get URLs!")
	}
	ctx.IndentedJSON(http.StatusOK, urls)
	return
}
func (ctrl *Link) GetAllLinksUser(ctx *gin.Context) {
	jwtClaims := jwt.ExtractClaims(ctx)
	username := jwtClaims["username"].(string)
	url, err := ctrl.svc.GetAllLinksUsers(username)
	if err != nil {
		log.Fatal("Unable to get Urls from users!")
	}
	ctx.IndentedJSON(http.StatusOK, url)
	return
}

func (ctrl *Link) GetShortUrl(ctx *gin.Context) {
	short_url := ctx.Param("short_url")
	url, err := ctrl.svc.GetShortUrl(short_url)
	if err != nil {
		log.Fatal("Unable to get Url!")
	}
	ctx.IndentedJSON(http.StatusOK, url)
	return
}

func (ctrl *Link) CreateCustomUrl(ctx *gin.Context) {
	short_url := ctx.Param("short_url")
	long_url := ctx.Param("long_url")
	if ctrl.svc.CheckShortUrl(short_url) {
		log.Fatal("Short URL already exist")
	}
	jwtClaims := jwt.ExtractClaims(ctx)
	username := jwtClaims["username"].(string)

	shortUrl, err := ctrl.svc.CreateCustomUrl(short_url, long_url, username)
	if err != nil {
		log.Fatal("Unable to cusomize!")
	}
	ctx.IndentedJSON(http.StatusOK, shortUrl)
	return
}
func (ctrl *Link) CreatePublicUrl(ctx *gin.Context) {
	var urlCreate UrlForm
	err := ctx.ShouldBindJSON(&urlCreate)
	fmt.Println(urlCreate)
	if err != nil {
		ctx.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	fmt.Println(urlCreate)

	shortUrl, err := ctrl.svc.CreatePublicUrl(urlCreate.Long_url)
	if err != nil {
		log.Fatal("Unable to cusomize!")
	}
	ctx.IndentedJSON(http.StatusCreated, shortUrl)
	return
}
