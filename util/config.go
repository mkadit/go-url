package util

import (
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
)

type Config struct {
	ServerPort string
	DBUser     string
	DBPassword string
	DBName     string
	DBHost     string
	DBPort     string
}

type TokenConfig struct {
	AccessTokenExpireTime time.Duration
	AccessTokenSecretKey  string
}

func LoadConfig() (config Config, err error) {
	err = godotenv.Load()
	if err != nil {
		log.Fatal("Can't load envinronment via .env file", err)
	}
	config = Config{
		os.Getenv("SERVER_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
	}
	return
}

func LoadTokenConfig() (tokenConfig TokenConfig, err error) {
	tokenConfig = TokenConfig{
		time.Minute * 5,
		os.Getenv("SECRET_KEY"),
	}
	return

}
