package util

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
	database "gitlab.com/mkadit/go-url/database/sqlc"
)

// Steps:
// 1. Create User
// 2. Craete jwt token
// 3. jwt token will be used when accessing url or website
// Website requiring login will ask for jwt token

type JwtClaim struct {
	Id int `json:"id"`
	jwt.RegisteredClaims
}

func CreateAccessToken(config TokenConfig, user database.User) (token string, err error) {
	registeredClaim := jwt.RegisteredClaims{}
	if config.AccessTokenExpireTime != 0 {
		registeredClaim = jwt.RegisteredClaims{
			ExpiresAt: &jwt.NumericDate{Time: time.Now().Add(config.AccessTokenExpireTime)},
		}
	}
	claims := JwtClaim{
		Id:               int(user.ID),
		RegisteredClaims: registeredClaim,
	}
	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
    fmt.Println(rawToken)
    fmt.Println(config.AccessTokenExpireTime)
    fmt.Println(config.AccessTokenSecretKey)
	token, err = rawToken.SignedString([]byte(config.AccessTokenSecretKey))
	if err != nil {
		err = fmt.Errorf("Token signing error: %w", err)
	}
	return

}
