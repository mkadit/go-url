-- name: GetUsers :many
SELECT * FROM users;

-- name: GetUserById :one
SELECT * FROM users
WHERE id = $1 LIMIT 1;

-- name: GetUserByUsername :one
SELECT * FROM users
WHERE username = $1 LIMIT 1;

-- name: DeleteUser :exec
DELETE FROM users
WHERE id = $1;

-- name: CreateUser :one
INSERT INTO users(username, password)
VALUES ($1, $2) RETURNING *;


-- name: GetAllLinksUser :many
SELECT long_url, short_url from users, urls
WHERE users.id = urls.users
AND users.username = $1;

-- name: CreateShortUrl :one
INSERT INTO urls(long_url, short_url, users)
VALUES ($1, $2, $3) RETURNING *;

-- name: GetShortUrl :one
SELECT * FROM urls
WHERE short_url = $1 LIMIT 1;

-- name: GetAllLinksPublic :many
SELECT * from urls
WHERE users IS NULL;

-- name: GetLatestId :one
SELECT id FROM urls 
ORDER BY id DESC
LIMIT 1;
