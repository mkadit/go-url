CREATE TABLE users(
	id SERIAL PRIMARY KEY,
    username VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL
);

CREATE TABLE urls(
    id SERIAL PRIMARY KEY,
    long_url VARCHAR(512) NOT NULL,
    short_url VARCHAR(256) NOT NULL UNIQUE,
    users int REFERENCES users(id) 
);
