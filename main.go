package main

import (
	"database/sql"
	"fmt"

	// "fmt"
	"log"

	"gitlab.com/mkadit/go-url/api"
	database "gitlab.com/mkadit/go-url/database/sqlc"
	"gitlab.com/mkadit/go-url/util"

	_ "github.com/lib/pq"
)

func main() {
	// err := util.SetLogger() // Logs
	// if err != nil {
	// 	log.Fatal("Logger cannto be set", err)
	// }
	config, err := util.LoadConfig()
	uri := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", config.DBUser, config.DBPassword, config.DBName, config.DBHost, config.DBPort)
	conn, err := sql.Open("postgres", uri)
	if err != nil {
		log.Fatal(err)
	}
	db := database.New(conn)
    _ = db
	// user, err := db.CreateUser(context.Background(), database.CreateUserParams{
	// 	Username: "testu3",
	// 	Password: "testp3",
	// })


	if err != nil {
		log.Fatal(err)
	}
    server := api.CreateServer(config, db)
    server.RunServer()
}
