package service

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	database "gitlab.com/mkadit/go-url/database/sqlc"
	"gitlab.com/mkadit/go-url/util"
	"golang.org/x/crypto/bcrypt"
)

type Link struct {
	db *database.Queries
}

func NewLinkService(db *database.Queries) *Link {
	return &Link{db}
}

func (svc *Link) GetUsers() ([]database.User, error) {
	return svc.db.GetUsers(context.Background())
}

func (svc *Link) GetUserByName(username string) (database.User, error) {
	return svc.db.GetUserByUsername(context.Background(), username)
}

func (svc *Link) CreateUser(username string, password string) (database.User, error) {

	hashedPassword, err := util.HashPassword(password)
	// fmt.Println(util.CheckPassword(hashedPassword, password))
	if err != nil {
		return database.User{}, err
	}
	userData := database.CreateUserParams{
		Username: username,
		Password: hashedPassword,
	}

	return svc.db.CreateUser(context.Background(), userData)
}

func (svc *Link) DeleteUser(username string) error {
	user, err := svc.db.GetUserByUsername(context.Background(), username)
	if err != nil {
		return err
	}
	svc.db.DeleteUser(context.Background(), user.ID)
	return nil
}

func (svc *Link) GetAllLinksPublic() ([]database.Url, error) {
	return svc.db.GetAllLinksPublic(context.Background())
}

func (svc *Link) GetAllLinksUsers(username string) ([]database.GetAllLinksUserRow, error) {
	return svc.db.GetAllLinksUser(context.Background(), username)
}

func (svc *Link) GetShortUrl(shortUrl string) (database.Url, error) {
	return svc.db.GetShortUrl(context.Background(), shortUrl)
}

func (svc *Link) CreateCustomUrl(short_url string, long_url string, username string) (database.Url, error) {

	user, err := svc.db.GetUserByUsername(context.Background(), username)
	if err != nil {
		log.Fatal("Fuck you!")
	}
	shortUrl := database.CreateShortUrlParams{
		LongUrl:  long_url,
		ShortUrl: short_url,
		Users:    sql.NullInt32{Int32: user.ID, Valid: true},
	}
	return svc.db.CreateShortUrl(context.Background(), shortUrl)
}

func (svc *Link) CreatePublicUrl(long_url string) (database.Url, error) {

	id, err := svc.db.GetLatestId(context.Background())
	short_url := idToShortUrl(int32(int(id) + 1))
	for err == nil {
		id += 1
		short_url = idToShortUrl(int32(int(id) + 1))
		_, err = svc.GetShortUrl(short_url)

	}

	shortUrl := database.CreateShortUrlParams{
		LongUrl:  long_url,
		ShortUrl: short_url,
		Users:    sql.NullInt32{Int32: 0, Valid: false},
	}
	return svc.db.CreateShortUrl(context.Background(), shortUrl)
}
func (svc *Link) CheckShortUrl(shortUrl string) bool {
	exist, _ := svc.GetShortUrl(shortUrl)
	if exist != (database.Url{}) {
		log.Fatal("Short URL already exist")
	}
	return false
}

func (svc *Link) LoginCheck(username string, password string) (database.User, bool, error) {
	tokenConfig, err := util.LoadTokenConfig()
	if err != nil {
		log.Fatal("Unable to load token config")
	}
	user, err := svc.GetUserByName(username)
	fmt.Println(err)
	if err != nil {
		return database.User{}, false, err
	}
	err = util.CheckPassword(password, user.Password)
	fmt.Println(err)
	if (err != nil) && (err == bcrypt.ErrMismatchedHashAndPassword) {
		return database.User{}, false, err
	}
	token, err := util.CreateAccessToken(tokenConfig, user)
	fmt.Println(token)
	if err != nil {
		return database.User{}, false, err
	}

	return user, true, err
}
