package service

func idToShortUrl(n int32) (short_link string) {
	array := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	for n > 0 {
		short_link += string(array[n%65])
		n /= 65
	}
	return reverseString(short_link)
}

func reverseString(s string) string {
	rns := []rune(s)
	for i, j := 0, len(rns)-1; i < j; i, j = i+1, j-1 {

		rns[i], rns[j] = rns[j], rns[i]
	}

	return string(rns)
}
