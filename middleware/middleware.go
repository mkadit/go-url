package middleware

import (
	"fmt"
	"log"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	database "gitlab.com/mkadit/go-url/database/sqlc"
	"gitlab.com/mkadit/go-url/service"
	"gitlab.com/mkadit/go-url/util"
)

type Link struct {
	svc *service.Link
}

func NewMiddleware(svc *service.Link) *Link {
	return &Link{svc}
}

func (ctrl *Link) NewJWTMiddleware() (middleware *jwt.GinJWTMiddleware) {
	tokenConfig, err := util.LoadTokenConfig()
	if err != nil {
		log.Fatal(err)
	}
	identityKey := "username"
	middleware, err = jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "test zone",
		Key:         []byte(tokenConfig.AccessTokenSecretKey),
		IdentityKey: identityKey,
		Timeout:     time.Hour * 24,
		MaxRefresh:  time.Hour * 24,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*database.User); ok {
				return jwt.MapClaims{
					identityKey: v.Username,
					"ID":        v.ID,
				}
			}
			return jwt.MapClaims{}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals database.CreateUserParams
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			username := loginVals.Username
			password := loginVals.Password

			user, userTrue, err := ctrl.svc.LoginCheck(username, password)
			fmt.Println(user)
			fmt.Println(userTrue)
			if err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			if userTrue {
				return &database.User{
					Username: user.Username,
					ID:       user.ID,
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &database.User{
				Username: claims[identityKey].(string),
			}

		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "header:token",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc:       time.Now,
		SendCookie:     true,
		SecureCookie:   false,
		CookieHTTPOnly: true,
		// CookieName:     "token",
	})
	return
}
