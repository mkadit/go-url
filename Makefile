run:
	go run main.go

up:
	docker-compose up --build -d

down:
	docker-compose down

air:
	air

build:
	go build -o main main.go 

logs:
	docker-compose	 -f -t

migrate:
	sqlc generate

.PHONY: run up air build logs migrate
