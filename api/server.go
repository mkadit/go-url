package api

import (
	"fmt"

	"github.com/gin-gonic/gin"
	database "gitlab.com/mkadit/go-url/database/sqlc"
	"gitlab.com/mkadit/go-url/util"
)

// Server struct
type Server struct{
	config util.Config
	router *gin.Engine
    db *database.Queries
}

// Server creation
func CreateServer(config util.Config, db *database.Queries) Server{
	router := gin.Default()
	server := Server{
		config,
		router,
        db,
	}

	return server
}

func (server *Server) RunServer(){
    server.SetupRouter()
	port := fmt.Sprintf(":%s", server.config.ServerPort)
	server.router.Run(port)
}
