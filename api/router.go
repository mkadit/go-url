package api

import (
	"github.com/gin-contrib/cors"
	"gitlab.com/mkadit/go-url/controller"
	"gitlab.com/mkadit/go-url/middleware"
	"gitlab.com/mkadit/go-url/service"
)

// Flow for events
//This file (setting routes) -->Controller (output *gin.Context) --> Service <T>
func (server *Server) SetupRouter() {
	// TODO: Figure out how to use the logs?
	// TODO: Find a way to keep connection to database even when it died
	//
	// server.router.Use(cors.Default())
	server.router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "OPTIONS", "PUT"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "User-Agent", "Referrer", "Host", "Token", "Set-Cookie", "Authorization", "Bearer", },
		ExposeHeaders:    []string{"Content-Length", "Set-Cookie", "Authorization", "Bearer"},
		AllowCredentials: true,
		AllowAllOrigins:  false,
		AllowOriginFunc:  func(origin string) bool { return true },
		MaxAge:           86400,
	}))

	linkService := service.NewLinkService(server.db)
	linkController := controller.NewLinkController(linkService)
	middlewareHandler := middleware.NewMiddleware(linkService).NewJWTMiddleware()

	api := server.router.Group("/api")
	api.POST("/register", linkController.CreateUser)
	api.POST("/login", middlewareHandler.LoginHandler)
	api.POST("/logout", middlewareHandler.LogoutHandler)

	//User related
	users := api.Group("/users", middlewareHandler.MiddlewareFunc())
	// user.use(tokenConfig, "user")
	users.GET("", linkController.GetUsers)
	users.DELETE(":username", linkController.DeleteUser)

	user := api.Group("/user", middlewareHandler.MiddlewareFunc())
	user.GET("", linkController.GetUser)
	user.GET("/urls", linkController.GetAllLinksUser)
	user.GET("/hello", linkController.Hello)
	user.POST("/:long_url/:short_url", linkController.CreateCustomUrl)

	urls := api.Group("/urls")
	urls.GET("", linkController.GetAllLinksPublic)
	urls.POST("", linkController.CreatePublicUrl)
	s := api.Group("/s")
	s.GET(":short_url", linkController.GetShortUrl)
}
